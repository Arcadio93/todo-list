Meteor.publish("allUserData", function () {
  return Meteor.users.find({});
});
Meteor.publish("tasks", function() {
  return Tasks.find();
});

Meteor.methods({
  addTask: function(task) {
    Tasks.insert({
      task: task,
      created: new Date(),
      done: false
    });
  },
  markTaskAsDone: function(id) {
    Tasks.update({
      _id: id
    }, {
      $set: {done: true}
    });
  },
  markTaskAsNotDone: function(id) {
    Tasks.update({
      _id: id
    }, {
      $set: {done: false}
    });
  },
  deleteTask: function(id) {
    Tasks.remove({
      _id: id
    });
  }
});
