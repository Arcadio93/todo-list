if (Meteor.isClient) {
  Meteor.subscribe("allUserData");
  Meteor.subscribe("tasks");
};

Router.configure({
  layoutTemplate: 'main'
});

Router.route('/', {
  name: 'todo',
  template: 'todo'
});
