Template.todo.helpers({
  tasks: function() {
    return Tasks.find({}, {sort: {created: -1}});
  },
  isDone: function() {
    return Tasks.findOne(this._id).done;
  }
});

Template.todo.events({
  'click #add_task': function() {
    var task = $('#task').val();

    Meteor.call('addTask', task);

    $('#task').val('');
  },
  'click #make_it_done': function() {
    Meteor.call('markTaskAsDone', this._id);
  },
  'click #make_it_not_done': function() {
    Meteor.call('markTaskAsNotDone', this._id);
  },
  'click #delete_task': function() {
    Meteor.call('deleteTask', this._id);
  }
});
